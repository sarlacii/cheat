Cheat (Bash)
=============

A cheatsheet viewer written in Bash.

This command displays "cheatsheets" for Linux and Unix commands.
Cheatsheets aren't manual pages, but examples of typical uses of common commands.

## History

This is a simple implementation of [Cheat](https://github.com/cheat/cheat), and uses community-generated [cheatsheets](https://github.com/cheat/cheatsheets) from that project.

Features:

* Written in Bash: easy to understand, easy to help maintain.
* Simple: all it does is display cheat sheets in your terminal.
* Customization: uses environment variables (like `PAGER`) so cheatsheets feel as natural as `man` and `info`.
* Fast: as fast as `cat`, `more` or `less`.

## Install

```
$ ./configure
$ make
$ sudo make install
```

By default, cheatsheets are downloaded to `/usr/local/share/cheat` so they are available to all users.
If you do not have root permissions, or you just want to install cheatsheets for yourself, set `CHEATPATH` to a location you can access.

For instance, add this to your `~/.bashrc` file to install all cheatsheets in your home directory:

```
export CHEATPATH=$HOME/.local/share/cheat
```

## Install to home directory

If you don't have administrative permissions on the computer you're using, you can install `cheat` to your home directory.

```
$ ./configure --prefix=$HOME/.local
$ make
$ make install
```

## Fetch cheatsheets

```
$ cheat --fetch
```

### List cheatsheets

```
$ cheat --list | less
7z
ab
acl
alias
ansi
[...]
```

### View a cheatsheet

```
$ cheat tar
# To extract an uncompressed archive:
tar -xvf /path/to/foo.tar

# To create an uncompressed archive:
tar -cvf /path/to/foo.tar /path/to/foo/

[...]
```

## Bug reports

Report bugs on Gitlab or by email.
